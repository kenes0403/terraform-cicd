terraform {
  backend "s3" {
    bucket = "backendtfkenessary"
    key    = "state"
    region = "us-east-1"
    dynamodb_table = "backend"
  }
}
